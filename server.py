import threading
from flask import Flask, request, abort
from gpiozero import PWMOutputDevice, LED, Buzzer, RGBLED
from colorzero import Color
import json
from time import sleep
import os
from subprocess import call, Popen, PIPE, STDOUT, check_call

app = Flask(__name__)

authKey = "2sMUxEN1xF"
appKey = ""

forwardLeft = PWMOutputDevice("GPIO12", True, 0, 500000, None)
reverseLeft = PWMOutputDevice("GPIO18", True, 0, 500000, None)
forwardRight = PWMOutputDevice("GPIO13", True, 0, 500000, None)
reverseRight = PWMOutputDevice("GPIO19", True, 0, 500000, None)

piezoPin = Buzzer("GPIO26")
piezoPin.off()

RGBLed = RGBLED("GPIO16", "GPIO20", "GPIO21", True, (0,0,0), True, None)
RGBLed.off()
RGBLed.value = (0,1,0)

motorFadeInTime = 1 #smooth start
motorFadeOutTime = 1 #smooth stop

forwardPWMR = 0
reversePWMR = 0
forwardPWML = 0
reversePWML = 0

stopStartDelay = 0.1 # 100 miliseconds
isSmoothTurning = False
runContinuousBeep = False

dbJSONFileName = '/home/pi/medbot/server/db_json.txt'
json_key_appkey = "app_key"
json_key_fpwmr = "fpwmr"
json_key_fpwml = "fpwml"
json_key_rpwml = "rpwml"
json_key_rpwmr = "rpwmr"
dbJson = {}

def stopBot():
    global runContinuousBeep
    runContinuousBeep = False

    forwardLeft.off()
    reverseLeft.off()
    forwardRight.off()
    reverseRight.off()

stopBot()

def checkIsStopped():
    return (forwardLeft.value == 0 and reverseLeft.value == 0 and forwardRight.value == 0 and reverseRight.value == 0)

@app.before_first_request
def loop_thread():
    def run():
        global runContinuousBeep, piezoPin
        while True:
            if runContinuousBeep:
                print("keep beeping!")
                piezoPin.on()
                sleep(0.2)
                piezoPin.off()
                sleep(1)

    thread = threading.Thread(target=run)
    thread.start()

@app.before_request
def before_request():
    global dbJson, forwardPWMR, reversePWMR, forwardPWML, reversePWML
    jsonbdFile = open(dbJSONFileName, 'r')
    dbJson = json.load(jsonbdFile)
    forwardPWMR = dbJson[json_key_fpwmr]
    reversePWMR = dbJson[json_key_rpwmr]
    forwardPWML = dbJson[json_key_fpwml]
    reversePWML = dbJson[json_key_rpwml]
    print("before request called " + str(forwardPWMR))
    jsonbdFile.close()

@app.route('/')
def index():
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        return 'Welcome to MedBot'
    else:
        abort(401, 'auth failed')

@app.route('/con_check', methods=['GET', 'POST'])
def conCheck():
    global RGBLed
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        RGBLed.value = (0,1,0)
        return "con success"
    else:
        abort(401, 'auth failed')

@app.route('/pair_controller', methods=['GET', 'POST'])
def pairController():
    global appKey, dbJson, piezoPin
    request_header = request.headers.get('MDAuthCheck')
    adminAuthPair = request.args.get("admin_auth")
    if validateRequest(request_header):
        appKey = request.args.get("app_key")

        if adminAuthPair is not None:
            if adminAuthPair == "yes":
                # write appKey to JSON
                dbJson[json_key_appkey] = appKey
                writeToDB(dbJson)
                
                # 2 beeps
                piezoPin.beep(0.2, 0.2, 2, False)
                    
                return "pairing success"
        else:
            if appKey is not None and appKey != "" and appKey != request.args.get("app_key"):
                return "already paired"
            else:
                if request.args.get("app_key") is not None:
                    appKey = request.args.get("app_key")

                    # write JSON
                    dbJson[json_key_appkey] = appKey
                    writeToDB(dbJson)

                    # 2 beeps
                    piezoPin.beep(0.2, 0.2, 2, False)

                    return "pairing success"
                else:
                    return "key not found"
       
    else:
        stopBot()
        abort(401, 'auth failed')

@app.route('/save_settings', methods=['GET', 'POST'])
def saveSettings():
    global forwardPWML, forwardPWMR, reversePWML, reversePWMR, dbJson
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        if request.args.get('f_pwm_l') is not None:
            forwardPWML = float(request.args.get('f_pwm_l'))
            dbJson[json_key_fpwml] = forwardPWML

        if request.args.get('f_pwm_r') is not None:
            forwardPWMR = float(request.args.get('f_pwm_r'))
            dbJson[json_key_fpwmr] = forwardPWMR

        if request.args.get('b_pwm_l') is not None:
            reversePWML = float(request.args.get('b_pwm_l'))
            dbJson[json_key_rpwml] = reversePWML

        if request.args.get('b_pwm_r') is not None:
            reversePWMR = float(request.args.get('b_pwm_r'))
            dbJson[json_key_rpwmr] = reversePWMR

        writeToDB(dbJson)

        return "pwm set success"
    else:
        abort(401, 'auth failed')

@app.route('/go_forward', methods=['GET', 'POST'])
def goForward():
    global isSmoothTurning, runContinuousBeep, forwardPWML, forwardPWMR
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        stopBot()
        if isSmoothTurning:
            sleep(stopStartDelay)
        
        isSmoothTurning = False

        for k in range(11):
            delta = 5
            forwardLeft.value = round(float(forwardPWML + (delta*k-50))/255, 3)
            reverseLeft.value = 0
            forwardRight.value = round(float(forwardPWMR + (delta*k-50))/255, 3)
            reverseRight.value = 0
            sleep(10/1000)

        runContinuousBeep = True

        print("Forward")
        print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))

        return "go forward"
    else:
        abort(401, 'auth failed')

@app.route('/go_backward', methods=['GET', 'POST'])
def goBackwards():
    global isSmoothTurning, runContinuousBeep, reversePWML, reversePWMR
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        stopBot()
        if isSmoothTurning :
            sleep(stopStartDelay)
        
        isSmoothTurning = False

        forwardLeft.value = 0
        reverseLeft.value = round(float(reversePWML)/255, 3)
        forwardRight.value = 0
        reverseRight.value = round(float(reversePWMR)/255, 3)

        runContinuousBeep = True

        print("Backward")
        print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))

        return "go backward"
    else:
        abort(401, 'auth failed')

@app.route('/turn_left', methods=['GET', 'POST'])
def turnLeft():
    global isSmoothTurning, runContinuousBeep, forwardPWML, forwardPWMR, reversePWML, reversePWMR
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        smoothTurnArg = request.args.get('smooth_turn')
        if smoothTurnArg is not None and smoothTurnArg == "true":
            isSmoothTurning = True
            turnDir = request.args.get('direction')

            turningDeviation = calcSmoothTurningDeviation()
            turningDeviationHigh = turningDeviation[0]
            turningDeviationLow = turningDeviation[1]

            if turnDir is not None and turnDir == "f":
                forwardLeft.value = round(float(forwardPWML-turningDeviationHigh)/255, 3)
                reverseLeft.value = 0
                forwardRight.value = round(float(forwardPWMR-turningDeviationLow)/255, 3)
                reverseRight.value = 0

                print("Turn Left smooth F")
                print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))
            else:
                forwardLeft.value = 0
                reverseLeft.value = round(float(reversePWML-turningDeviationHigh)/255, 3)
                forwardRight.value = 0
                reverseRight.value = round(float(reversePWMR-turningDeviationLow)/255, 3)

                print("Turn Left smooth B")
                print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))
        else:
            forwardLeft.value = 0
            reverseLeft.value =  round(float(forwardPWML-70)/255, 3)
            forwardRight.value =  round(float(forwardPWMR-70)/255, 3)
            reverseRight.value = 0

            print("Turn Left")
            print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))

        runContinuousBeep = True

        return "turn left"
    else:
        abort(401, 'auth failed')

@app.route('/turn_right', methods=['GET', 'POST'])
def turnRight():
    global isSmoothTurning, runContinuousBeep, forwardPWML, forwardPWMR, reversePWMR, reversePWML
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        smoothTurnArg = request.args.get('smooth_turn')
        if smoothTurnArg is not None and smoothTurnArg == "true":
            isSmoothTurning = True
            turnDir = request.args.get('direction')

            turningDeviation = calcSmoothTurningDeviation()
            turningDeviationHigh = turningDeviation[0]
            turningDeviationLow = turningDeviation[1]

            if turnDir is not None and turnDir == "f":
                forwardLeft.value = round(float(forwardPWML-turningDeviationLow)/255, 3)
                reverseLeft.value = 0
                forwardRight.value = round(float(forwardPWMR-turningDeviationHigh)/255, 3)
                reverseRight.value = 0

                print("Turn Right smooth F")
                print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))
            else:
                forwardLeft.value = 0
                reverseLeft.value = round(float(reversePWML-turningDeviationLow)/255, 3)
                forwardRight.value = 0
                reverseRight.value = round(float(reversePWMR-turningDeviationHigh)/255, 3)

                print("Turn Right smooth B")
                print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))
        else:
            forwardLeft.value = round(float(forwardPWML-50)/255, 3)
            reverseLeft.value =  0
            forwardRight.value =  0
            reverseRight.value = round(float(forwardPWMR-50)/255, 3)

            print("Turn Right")
            print("fl: " + str(forwardLeft.value) + " rl: " + str(reverseLeft.value) + " fr: " + str(forwardRight.value) + " rr: " + str(reverseRight.value))

        runContinuousBeep = True

        return "turn right"
    else:
        abort(401, 'auth failed')

@app.route('/stop', methods=['GET', 'POST'])
def stop():
    stopBot()
    return "stop"

@app.route('/is_stopped', methods=['GET'])
def isStopped():
    if checkIsStopped():
        return 'yes'
    else:
        return 'no'


@app.route('/reboot', methods=['POST'])
def reboot():
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        RGBLed.value = (1,0,0)
        piezoPin.beep(0.1, 0.1, 3, False)

        return "rebooting"
        os.system("sudo reboot")
    else:
        abort(401, 'auth failed')

@app.route('/cam_action', methods=['POST'])
def camOnOFf():
    request_header = request.headers.get('MDAuthCheck')
    if validateRequest(request_header):
        camAction = request.args.get('action')
        if camAction == "start":
            try:
                check_call(["service", "motion", "start"])
                return "cam start ok"
            except Exception as e:
                return "failed"

        elif camAction == "restart":
            call("sudo service motion restart", shell=True)
            sleep(0.2)
        elif camAction == "reload":
            call("sudo service motion reload", shell=True)
            sleep(0.2)
        else:
            call("sudo service motion stop", shell=True)
            sleep(0.2)

        return "ok"
    else:
        abort(401, 'auth failed')


def validateRequest(request_header):
    return(request_header == authKey)

def writeToDB(dbJson):
    with open(dbJSONFileName, 'w') as db_json:
        json.dump(dbJson, db_json)


def calcSmoothTurningDeviation():
    global forwardPWMR, reversePWMR, forwardPWML, reversePWML
    if forwardPWML < 150:
        return (30, 0) # (high, low)
    elif(forwardPWML >= 150 and forwardPWML < 200):
        return (50, 10)
    else:
        return (60, 20)


app.run(debug=True, host='0.0.0.0', port=8090, threaded=True)